import graphene
import mock
import pytest
from asgiref.sync import sync_to_async
from channels.testing import WebsocketCommunicator
from django.db.models.signals import post_save
from graphene.test import Client
from graphene_subscriptions.consumers import GraphqlSubscriptionConsumer

from temperature.models import Temperature
from temperature.schema import Query


@pytest.mark.django_db
def test_temperature_query():
    """ Testing temperature graphene query """
    value = 50.6
    Temperature.objects.create(value=value)
    schema = graphene.Schema(query=Query)
    client = Client(schema)

    graphql_query = """query {
      currentTemperature
      {
        timestamp,
        value,
        unit
      }
    }"""

    executed = client.execute(graphql_query)

    print(executed)
    assert executed["data"]["currentTemperature"]["value"] == value
    assert executed["data"]["currentTemperature"]["unit"] == "KELVIN"


@pytest.mark.django_db
def test_temperature_signal_called():
    """ Testing temperature signals """
    with mock.patch('graphene_subscriptions.signals.post_save_subscription', autospec=True) as mocked_handler:
        post_save.connect(mocked_handler, sender=Temperature, dispatch_uid='test_cache_mocked_handler')
        # do stuff that will call the post_save of User
        Temperature.objects.create(value=150)
    assert mocked_handler.call_count == 1


async def query(graphql_query, communicator, variables=None):
    await communicator.send_json_to(
        {"id": 1, "type": "start", "payload": {"query": graphql_query, "variables": variables}}
    )


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_temperature_subscription():
    """ Testing temperature graphene subscription """
    subscription = """
            subscription {
                currentTemperatureSubscribe
                {
                    timestamp,
                    value,
                    unit
                }
            }
        """
    communicator = WebsocketCommunicator(GraphqlSubscriptionConsumer, "/graphql/")
    connected, _subprotocol = await communicator.connect()
    assert connected
    await query(subscription, communicator)

    value = 42
    await sync_to_async(Temperature.objects.create)(value=value)
    response = await communicator.receive_json_from()

    print(response)
    assert response["payload"]["data"]["currentTemperatureSubscribe"]["value"] == value
