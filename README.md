# lo_coding_test

This is the readme of the coding test project done for Loft Orbital.

Author : Lionel Duroyon <lionel.duroyon@gmail.com>

# Installation

Clone the project : 

```git clone git@gitlab.com:lionel.duroyon1/lo_coding_test.git```

Build docker image :

```cd lo_coding_test```

```docker build . --tag loft_orbital``` 

Launch docker image :

```docker run --publish 8001:8000 loft_orbital```

# Usage

Connect to ```127.0.0.1:8001/graphql``` to view the webserver graphiql interface where you can test query ( not the subscription cf below )

Launching tests :

```
python manage.py makemigrations
python manage.py migrate
pytest tests
```


# Warning about graphiql and subscription

Current graphiql doesn't seem to support subscriptions.

But I have successfully tested the subscription with pytest ( cf ```tests/temperature/test_temperature.py``` ).

# What have been done

We have implemented the following graphql API exposing temperature information.

## Query

Description : get the currentTemperature ( = latest )

```
query
{
    currentTemperature
    {
        timestamp
        value
        unit
    }
}
```

## Subscription

Description : subscribe to get temperature when one temperature is measured ( = created )

```
subscription
{
    currentTemperatureSubscribe
    {
        temperature
        {
            timestamp
            value
            unit
        }
    }
}
```
