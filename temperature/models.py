""" Temperature models """
from django.db import models

# International System unit : Kelvin
KELVIN = "KELVIN"
CELSUS = "CELSUS"
FAHRENHEIT = "FAHRENHEIT"
RANKINE = "RANKINE"
REAUMUR = "REAUMUR"

TEMPERATURE_UNIT_CHOICES = [
        (KELVIN, 'kelvin'),
        (CELSUS, 'celsus'),
        (FAHRENHEIT, 'fahrenheit'),
        (RANKINE, "rankine"),
        (REAUMUR, "reaumur")
    ]


class Temperature(models.Model):
    """
    Represent a temperature measure at a given time, with a given unit
    """
    timestamp = models.DateTimeField(auto_now_add=True)
    value = models.FloatField()
    unit = models.CharField(
        max_length=20,
        choices=TEMPERATURE_UNIT_CHOICES,
        default=KELVIN,
    )

    def __str__(self):
        return f"{self.timestamp}-{self.value}-{self.unit}"
