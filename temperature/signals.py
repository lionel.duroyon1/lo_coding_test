""" Signals """
from django.db.models.signals import post_save
from graphene_subscriptions.signals import post_save_subscription

from temperature.models import Temperature

# Linking Temperature post_save to post_save_subscription
post_save.connect(post_save_subscription, sender=Temperature, dispatch_uid="temperature_post_save")
