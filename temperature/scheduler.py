import random
from apscheduler.schedulers.background import BackgroundScheduler
from temperature.models import Temperature

MIN_TEMP_VAL = -100
MAX_TEMP_VAL = 100
SIMULATION_INTERVAL = 10

scheduler = BackgroundScheduler(daemon=True)
JOB = None


def fake_temperature():
    """ Insert a temperature into database, simulating a temperature sensor """
    value = random.uniform(MIN_TEMP_VAL, MAX_TEMP_VAL)
    Temperature.objects.create(value=value)


def simulate_temperature(interval: int = SIMULATION_INTERVAL):
    """ Simulate temperature for a given interval """
    global JOB
    JOB = scheduler.add_job(fake_temperature, 'interval', seconds=interval)
    try:
        scheduler.start()
    except Exception as exception:
        print(exception)
