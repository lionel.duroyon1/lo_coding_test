""" Describe Temperature Schema """
import graphene
from graphene_django import DjangoObjectType
from graphene_subscriptions.events import CREATED

from temperature.models import Temperature


class TemperatureType(DjangoObjectType):
    """ Temperature Type """
    class Meta:
        model = Temperature


class Query(graphene.ObjectType):
    """ Temperature Query """
    current_temperature = graphene.Field(TemperatureType)

    def resolve_current_temperature(self, _info, **_kwargs):
        """ Resolve current temperature """
        return Temperature.objects.latest('timestamp')


class Subscription(graphene.ObjectType):
    """ Temperature Subscription """
    current_temperature_subscribe = graphene.Field(TemperatureType)

    def resolve_current_temperature_subscribe(root_value, _info):
        """ Handle current emperature subscription """
        return root_value.filter(
            lambda event:
            event.operation == CREATED and
            isinstance(event.instance, Temperature)
        ).map(lambda event: event.instance)


schema = graphene.Schema(
    query=Query,
    subscription=Subscription,
)
