from django.apps import AppConfig


class TemperatureConfig(AppConfig):
    name = 'temperature'

    def ready(self):
        import temperature.signals
        from .scheduler import simulate_temperature
        simulate_temperature()
