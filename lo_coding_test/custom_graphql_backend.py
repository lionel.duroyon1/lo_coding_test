""" Custom graphql backend """
from typing import Callable

from graphene_django.views import GraphQLView
from graphql.backend import GraphQLCoreBackend
from rx.core import ObservableBase


class GraphQLCustomCoreBackend(GraphQLCoreBackend):
    """ We define GraphQLCustomCoreBackend to set allow_subscriptions to True """
    def __init__(self, executor: Callable = None):
        super().__init__(executor)
        self.execute_params['allow_subscriptions'] = True


class GraphQLObservableUnboxingView(GraphQLView):
    """ We define this class to be able to use Observable from subscription """
    def execute_graphql_request(
            self,
            request: Callable,
            data: dict, query: str,
            variables: dict,
            operation_name: str,
            show_graphiql: bool = False
    ):
        target_result = None

        def override_target_result(value):
            nonlocal target_result
            target_result = value

        execution_result = super().execute_graphql_request(\
            request, data, query, variables, operation_name, show_graphiql)
        if execution_result:
            if isinstance(execution_result, ObservableBase):
                target = \
                    execution_result.subscribe(on_next=lambda value: override_target_result(value))
                target.dispose()
            else:
                return execution_result

        return target_result
