""" Describing temperature schema """
import graphene
import temperature.schema


class Query(temperature.schema.Query, graphene.ObjectType):
    """ Temperature Query """


class Subscription(temperature.schema.Subscription, graphene.ObjectType):
    """ Temperature Subscription """


schema = graphene.Schema(query=Query, subscription=Subscription)
