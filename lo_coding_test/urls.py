"""lo_coding_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path
from django.views.generic.base import RedirectView
from graphene_subscriptions.consumers import GraphqlSubscriptionConsumer
from lo_coding_test.custom_graphql_backend import \
    (GraphQLCustomCoreBackend, GraphQLObservableUnboxingView)
from lo_coding_test.schema import schema

urlpatterns = [
    path('', RedirectView.as_view(url='graphql/')),
    path('graphql/', GraphQLObservableUnboxingView.as_view(
        schema=schema, graphiql=True, backend=GraphQLCustomCoreBackend())),
]


# Associate GraphqlSubscriptionConsumer with graphql/ path
application = ProtocolTypeRouter({
    "websocket": URLRouter([
        path('graphql/', GraphqlSubscriptionConsumer)
    ]),
})
